package control;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Trabalho TDE2 que consiste em: Criar um programa que leia que um arquivo com
 * um número indefinido de linhas, onde cada uma destas linhas descreve uma
 * String. Escreva o algoritmo que coloque estas linhas em ordem e imprima na
 * tela as linhas ordenadamente. Obs* Por uma questão de organização, quebre o
 * seu código em métodos estáticos que são responsáveis por tarefas pontuais.
 * Assim evitamos que todo o código fique escrito somente no método main.
 * 
 * @author Valdecir
 */
public final class Programa {

	/**
	 * Constructor.
	 */
	private Programa() {
	}

	/**
	 * Método principal.
	 * 
	 * @param args
	 */
	public static void main(final String[] args) {

		List<String> lista = lerArquivo();

		for (String linha : lista)
			System.out.print(linha + " ");

		ordenaLista(lista);
		System.out.println();

		for (String linha : lista) 
			System.out.print(linha + " ");
	}

	/**
	 * Método que preenche um array de String com o conteúdo de um arquivo.
	 * 
	 * @return
	 */
	public static List<String> lerArquivo() {
		List<String> list = new ArrayList<String>();

		try {
			FileReader fr = new FileReader("arquivo.dat");
			BufferedReader br = new BufferedReader(fr);

			while (true) {
				String linha = br.readLine();

				if (linha == null)
					break;
				else 
					list.add(linha);
			}

			br.close();

		} catch (IOException e) {
			System.out.println("Erro na leitura do arquivo: " + e.getMessage());
		}

		return list;
	}

	/**
	 * Método auxiliar do método ordenaLista
	 * 
	 * @param posAtual
	 * @param lista
	 */
	public static void posicionaMenor(int posAtual, List<String> lista) {
		int posMenor = posAtual;

		for (int j = posAtual + 1; j < lista.size(); j++)
			if (lista.get(posMenor).compareTo(lista.get(j)) > 0)
				posMenor = j;

		String aux = lista.get(posAtual);
		lista.set(posAtual, lista.get(posMenor));
		lista.set(posMenor, aux);
	}

	/**
	 * Método que recebe uma Lista de Strings e a ordena
	 * 
	 * @param lista
	 */
	public static void ordenaLista(List<String> lista) {
		for (int i = 0; i < lista.size() - 1; i++)
			posicionaMenor(i, lista);
	}

}
